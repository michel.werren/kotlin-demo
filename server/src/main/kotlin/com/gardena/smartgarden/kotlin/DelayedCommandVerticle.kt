package com.gardena.smartgarden.kotlin

import com.gardena.smartgarden.kotlin.dto.DelayedCmd
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.util.*
import java.util.concurrent.TimeUnit

class DelayedCommandVerticle : CoroutineVerticle() {

    companion object {
        const val ADDRESS = "delayed-address"
    }

    private val random = Random()

    override suspend fun start() {
        vertx.eventBus().consumer<JsonObject>(ADDRESS, this::onMessage)
    }

    /**
     * Handler to reply on a message with a random delay within a boundry given by command dto
     */
    private fun onMessage(msg: Message<JsonObject>) {
        launch(vertx.dispatcher()) {
            val cmd = msg.body().mapTo(DelayedCmd::class.java)

            val startTimestamp = System.currentTimeMillis()
            delay(random.nextInt(cmd.maxDelay).toLong(), TimeUnit.SECONDS)

            val differenceInSeconds = System.currentTimeMillis().differenceInSeconds(startTimestamp)
            msg.reply(differenceInSeconds)

        }
    }
}