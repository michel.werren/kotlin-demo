package com.gardena.smartgarden.kotlin

import com.gardena.smartgarden.kotlin.dto.DelayedCmd
import com.gardena.smartgarden.kotlin.dto.DurationDto
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.awaitEvent
import io.vertx.kotlin.coroutines.awaitResult
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory

/**
 * Handler for delayed command requests
 */
class RequestHandler(private val vertx: Vertx) : Handler<RoutingContext> {
    private val log = LoggerFactory.getLogger(RequestHandler::class.java)

    override fun handle(rc: RoutingContext) {
        launch(vertx.dispatcher()) {

            // Read the body
            val bodyBuffer = awaitEvent<Buffer> { rc.request().bodyHandler(it) }
            val dtoJson = bodyBuffer.toJsonObject()
            val cmd = dtoJson.mapTo(DelayedCmd::class.java)

            val response = rc.response()
            try {
                val delayedSeconds = awaitResult<Message<Int>> { vertx.eventBus().send(DelayedCommandVerticle.ADDRESS, dtoJson, it) }
                val responseValue = JsonObject.mapFrom(DurationDto(delayedSeconds.body())).encode()
                response.setContentLength(responseValue)
                response.setJsonContentType()
                response.write(responseValue)
                log.info("Replied to request: ${cmd.requestNr} in ${delayedSeconds.body()} seconds")
            } catch (e: Exception) {
                log.error("Failed to execute delayed handler", e)
                response.statusCode = 500
                response.statusMessage = e.message
            }
            response.end()
        }
    }
}