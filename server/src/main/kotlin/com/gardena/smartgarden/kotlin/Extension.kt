package com.gardena.smartgarden.kotlin

import io.netty.handler.codec.http.HttpHeaderNames
import io.vertx.core.http.HttpServerResponse

/**
 * Subtract the given long from this and divide to get in seconds
 */
fun Long.differenceInSeconds(startInMillis: Long): Int {
    val difference = this - startInMillis
    return (difference / 1000).toInt()
}

/**
 * Add the content length header field to the request
 */
fun HttpServerResponse.setContentLength(value: String) {
    putHeader("Content-Length", "${value.toByteArray(charset("UTF-8")).size}")
}

/**
 * Add the content type header field to the request
 */
fun HttpServerResponse.setJsonContentType() {
    putHeader(HttpHeaderNames.CONTENT_TYPE, "application/json")
}