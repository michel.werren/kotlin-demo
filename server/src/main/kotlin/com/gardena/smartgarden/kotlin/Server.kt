package com.gardena.smartgarden.kotlin

import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.Json
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.kotlin.coroutines.awaitResult
import kotlinx.coroutines.experimental.runBlocking
import org.slf4j.LoggerFactory


class Server(private val vertx: Vertx, private val host: String, private val port: Int) {

    companion object {

        private val LOGGER = LoggerFactory.getLogger(Server::class.java)

        private val allowedHeaders = HashSet<String>().apply {
            add("x-requested-with")
            add("Access-Control-Allow-Origin")
            add("origin")
            add("Content-Type")
            add("accept")
            add("X-PINGARUNER")

        }

        private val allowedMethods = HashSet<HttpMethod>().apply {
            add(HttpMethod.GET)
            add(HttpMethod.POST)
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val vertx = Vertx.vertx()

            Runtime.getRuntime().addShutdownHook(Thread { vertx.close() })

            runBlocking { Server(vertx, "0.0.0.0", 8080).startServer() }
        }
    }

    /**
     * All tasks to start the Vertx server.
     */
    private suspend fun startServer() {

        makeJacksonKotlinReady()

        try {
            deployDelayedVerticle()
        } catch (e: Exception) {
            LOGGER.error("Unable to deploy delayed verticle")
        }

        val httpServer = vertx.createHttpServer(HttpServerOptions().setHost(host))
        val router = Router.router(vertx)

        router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods))

        router.post("/delayed").handler(RequestHandler(vertx))

        httpServer.requestHandler(router::accept).listen(port)

        LOGGER.info("Kotlin demo server started")
    }


    /**
     * Jackson need's an additional module to work with Kotlin code in some circumstances.
     */
    private fun makeJacksonKotlinReady() {
        Json.mapper.registerModule(KotlinModule())
    }

    /**
     * Just deploys the verticle that simulates slower command execution.
     */
    private suspend fun deployDelayedVerticle() {
        awaitResult<String> { vertx.deployVerticle(DelayedCommandVerticle::class.java.name, DeploymentOptions().setInstances(4), it) }
    }
}