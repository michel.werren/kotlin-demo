package com.gardena.smartgarden.kotlin.dto

expect class DurationDto(duration: Int)