package com.gardena.smartgarden.kotlin.shared

import com.gardena.smartgarden.kotlin.dto.DurationDto

expect class AverageDurationCalculator {
    companion object {
        fun calculateAverage(durations: List<DurationDto>): Double
    }
}