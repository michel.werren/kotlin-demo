package com.gardena.smartgarden.kotlin.dto

expect class DelayedCmd(maxDelay: Int, requestNr: Int)