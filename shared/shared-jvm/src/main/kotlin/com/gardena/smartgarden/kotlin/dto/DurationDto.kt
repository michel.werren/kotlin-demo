package com.gardena.smartgarden.kotlin.dto

import com.fasterxml.jackson.annotation.JsonCreator

actual class DurationDto @JsonCreator actual constructor(val duration: Int)