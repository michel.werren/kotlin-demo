package com.gardena.smartgarden.kotlin.dto

import com.fasterxml.jackson.annotation.JsonCreator

actual class DelayedCmd @JsonCreator actual constructor(val maxDelay: Int, val requestNr: Int)