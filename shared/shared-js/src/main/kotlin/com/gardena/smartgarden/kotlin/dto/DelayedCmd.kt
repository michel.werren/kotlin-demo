package com.gardena.smartgarden.kotlin.dto

actual class DelayedCmd actual constructor(val maxDelay: Int, val requestNr: Int)