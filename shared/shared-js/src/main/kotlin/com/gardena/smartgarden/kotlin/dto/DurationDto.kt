package com.gardena.smartgarden.kotlin.dto

actual class DurationDto actual constructor(val duration: Int)