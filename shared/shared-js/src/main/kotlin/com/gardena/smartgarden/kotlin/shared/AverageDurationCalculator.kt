package com.gardena.smartgarden.kotlin.shared

import com.gardena.smartgarden.kotlin.dto.DurationDto

actual class AverageDurationCalculator {
    actual companion object {
        actual fun calculateAverage(durations: List<DurationDto>): Double {
            var sum = 0.0
            durations.forEach { sum += it.duration }
            return sum / durations.size
        }
    }
}