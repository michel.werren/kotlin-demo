// Define environment dependent vars

let nodeEnv = 'development';
let restBasePath = 'http://localhost:8090/frontend';

if (defined.PRODUCTION === true || defined.PRODUCTION === "true") {
    nodeEnv = 'production';
    restBasePath = 'http://prod/frontend';
}

const modePlugin = new webpack.DefinePlugin({
    'process.env': {
        'NODE_ENV': JSON.stringify(nodeEnv)
    },
    'restBasePath': JSON.stringify(restBasePath),
    'production': defined.PRODUCTION === "true"
});
config.plugins.push(modePlugin);
