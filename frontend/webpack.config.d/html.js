// Generation of the production index.html

if (defined.PRODUCTION === true || defined.PRODUCTION === "true") {
    const HtmlWebpackPlugin = require('html-webpack-plugin');
    const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');

    config.plugins.push(new HtmlWebpackPlugin({
        title: "Scape Organizer",
        template: '../../environment/production/index.html',
        excludeAssets: [/style.*.js/]
    }));
    config.plugins.push(new HtmlWebpackExcludeAssetsPlugin());
}