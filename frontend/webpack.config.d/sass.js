// SASS to CSS compiler. In production mode the css will additionally extracted into a separate file.

config.entry.styles = "../../src/main/kotlin/com/gardena/smartgarden/kotlin/frontend/Main.scss";

const ExtractTextPlugin = require("extract-text-webpack-plugin");
if (defined.PRODUCTION === true || defined.PRODUCTION === "true") {

    const extractSass = new ExtractTextPlugin({
        filename: "[name].css",
        disable: false
    });

    config.module.rules.push({
        test: /\.scss$/,
        use: extractSass.extract({
            use: [{
                loader: "css-loader"
            }, {
                loader: "sass-loader"
            }],
            // use style-loader in development
            fallback: "style-loader"
        }),
    });

    config.plugins.push(extractSass);

} else {
    config.module.rules.push({
        test: /\.scss$/,
        use: [{
            loader: "style-loader"
        }, {
            loader: "css-loader"
        }, {
            loader: "sass-loader"
        }]
    });
}


