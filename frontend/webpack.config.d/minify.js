// Minify JS for production

if (defined.PRODUCTION === true || defined.PRODUCTION === "true") {
    const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
    config.plugins.push(new UglifyJSPlugin());
}