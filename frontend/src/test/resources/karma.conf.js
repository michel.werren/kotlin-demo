const webpackConfig = require("@build_folder@/webpack.config.js");
webpackConfig.mode = 'development';
webpackConfig.resolve.modules = ["js", "../../shared/shared-js/build/classes/kotlin/main", "@build_folder@/js/frontend.js", "resources/main", "@build_folder@/node_modules", "node_modules"];
webpackConfig.context = "@build_folder@/js";

module.exports = function (config) {
    config.set({
        "basePath": "@build_folder@",
        "frameworks": [
            "qunit"
        ],
        "reporters": [
            "progress",
            "junit"
        ],
        "files": [
            {pattern: '@build_folder@/node_modules/@babel/polyfill/browser.js', instrument: false},
            "@build_folder@/classes/kotlin/test/frontend_test.js"
        ],
        "exclude": [
            "*~",
            "*.swp",
            "*.swo"
        ],
        "port": 9876,
        "runnerPort": 9100,
        "colors": false,
        "autoWatch": true,
        "browsers": [
            "PhantomJS"
        ],
        "captureTimeout": 60000,
        "singleRun": false,
        "preprocessors": {
            "@build_folder@/classes/kotlin/test/frontend_test.js": [
                "sourcemap",
                "webpack"
            ]
        },
        "plugins": [
            "karma-phantomjs-launcher",
            "karma-junit-reporter",
            "karma-qunit",
            "karma-sourcemap-loader",
            "karma-webpack"
        ],
        "client": {
            "clearContext": false,
            "qunit": {
                "showUI": true,
                "testTimeout": 5000
            }
        },
        "junitReporter": {
            "outputFile": "@build_folder@/reports/karma.xml",
            "suite": "karma"
        },
        "webpack": webpackConfig
    })
};
