package com.gardena.smartgarden.kotlin.shared

import com.gardena.smartgarden.kotlin.dto.DurationDto
import kotlin.test.Test
import kotlin.test.assertEquals


class AverageDurationCalculatorTest {

    @Test
    fun calculation() {
        assertEquals(4.0, AverageDurationCalculator.calculateAverage(listOf(DurationDto(5), DurationDto(3))))
    }
}