package com.gardena.smartgarden.kotlin.frontend.app.welcome

import com.gardena.smartgarden.kotlin.frontend.component.titleComponent
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.a
import react.dom.div
import react.dom.span

/**
 * Component and navigation target for the path / context. Shows a welcome text and a navigation button
 */
class WelcomeComponent : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        div("welcome-component") {
            div("welcome-component-title") {
                titleComponent { attrs.title = "Welcome to the Kotlin Demo" }
            }
            div("welcome-component-content") {
                a(href = "#/duration") {
                    +"GoTo"
                }
                span("welcome-component-content-text") { +"to the coroutine demo" }
            }
        }
    }
}