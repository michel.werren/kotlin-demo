package com.gardena.smartgarden.kotlin.frontend

import com.gardena.smartgarden.kotlin.frontend.app.app
import com.gardena.smartgarden.kotlin.frontend.app.delay.DelayRequestComponent
import com.gardena.smartgarden.kotlin.frontend.app.welcome.WelcomeComponent
import klogging.KLoggers
import klogging.KLoggingLevels
import react.dom.render
import react.router.dom.hashRouter
import react.router.dom.route
import react.router.dom.switch
import kotlin.browser.document

external var production: Boolean

/**
 * Entry point of the whole application
 */
fun main(args: Array<String>) {

    if (production) {
        KLoggers.defaultLoggingLevel = KLoggingLevels.INFO
    } else {
        KLoggers.defaultLoggingLevel = KLoggingLevels.TRACE
    }

    val container = document.getElementById("root")!!
    render(container) {
        // Routing declaration
        hashRouter {
            // The app component is anytime parent, at any navigation target
            app {
                switch {
                    route("/", component = WelcomeComponent::class, exact = true)
                    route("/duration", component = DelayRequestComponent::class, exact = true)
                }
            }
        }
    }
}