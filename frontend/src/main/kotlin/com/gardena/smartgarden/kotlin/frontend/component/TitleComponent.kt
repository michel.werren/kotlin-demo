package com.gardena.smartgarden.kotlin.frontend.component

import react.*
import react.dom.h1

interface TitleComponentProps : RProps {
    var title: String?
}

/**
 * Component to show how to interact with custom, own components.
 * A component to just show a h2 with the given text.
 */
class TitleComponent : RComponent<TitleComponentProps, RState>() {
    override fun RBuilder.render() {
        h1 {
            props.title?.let {
                +it
            }
        }
    }
}

/**
 * Extension function from the Kotlin React DSL to declare an instance of this component.
 */
fun RBuilder.titleComponent(handler: RHandler<TitleComponentProps>) = child(TitleComponent::class, handler)