package com.gardena.smartgarden.kotlin.frontend.app

import react.*
import react.dom.div

/**
 * Parent component of the whole application.
 */
class App : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        div(classes = "demo-app") {
            props.children()
        }
    }
}

fun RBuilder.app(handler: RHandler<RProps>) = child(App::class, handler)