package com.gardena.smartgarden.kotlin.frontend

import org.w3c.xhr.JSON
import org.w3c.xhr.XMLHttpRequest
import org.w3c.xhr.XMLHttpRequestResponseType

/**
 * Extension function to apply the json content type header to a request.
 */
fun XMLHttpRequest.setJsonContentType(): XMLHttpRequest {
    responseType = XMLHttpRequestResponseType.JSON
    return this
}

/**
 * Extension function to apply the accept json header.
 */
fun XMLHttpRequest.setJsonAccept(): XMLHttpRequest {
    setRequestHeader("accept", "application/json")
    return this
}