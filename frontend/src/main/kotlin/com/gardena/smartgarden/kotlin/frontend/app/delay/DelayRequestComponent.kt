package com.gardena.smartgarden.kotlin.frontend.app.delay

import com.gardena.smartgarden.kotlin.dto.DelayedCmd
import com.gardena.smartgarden.kotlin.dto.DurationDto
import com.gardena.smartgarden.kotlin.frontend.component.titleComponent
import com.gardena.smartgarden.kotlin.frontend.setJsonAccept
import com.gardena.smartgarden.kotlin.frontend.setJsonContentType
import com.gardena.smartgarden.kotlin.shared.AverageDurationCalculator
import klogging.KLoggers
import kotlinext.js.clone
import kotlinx.coroutines.experimental.launch
import kotlinx.html.INPUT
import kotlinx.html.InputType
import kotlinx.html.id
import kotlinx.html.js.onClickFunction
import org.w3c.xhr.XMLHttpRequest
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.button
import react.dom.div
import react.dom.input
import react.dom.label
import kotlin.browser.window
import kotlin.coroutines.experimental.suspendCoroutine

interface DelayRequestComponentState : RState {
    var runningQueries: Int?
    var allDelays: List<DurationDto>?
}

/**
 * Show a mini form to simulate slow requests.
 */
class DelayRequestComponent : RComponent<RProps, DelayRequestComponentState>() {

    private val log = KLoggers.logger(DelayRequestComponent::class)

    override fun RBuilder.render() {
        var maxDelayInput: INPUT? = null
        var requestCountInput: INPUT? = null

        div("delayed-request-component") {
            titleComponent { attrs.title = "Slow request simulation" }
            div("delayed-request-component-input") {
                val idValue = "duration-input"

                label(classes = "delayed-request-component-input-label") {
                    attrs.htmlFor = idValue
                    +"Max. Duration: "
                }
                input(type = InputType.number) {
                    ref {
                        maxDelayInput = it
                    }

                    attrs {
                        id = idValue
                    }
                }
            }
            div("delayed-request-component-input") {
                val idValue = "request-count-input"

                label(classes = "delayed-request-component-input-label") {
                    attrs.htmlFor = idValue
                    +"Request count: "
                }
                input(type = InputType.number) {
                    ref {
                        requestCountInput = it
                    }

                    attrs {
                        id = idValue
                    }
                }
            }
            div("delayed-request-component-button") {
                button {
                    +"Query"
                    attrs.disabled = (state.runningQueries ?: 0) > 0
                    attrs.onClickFunction = {
                        onClick(maxDelayInput, requestCountInput)
                    }
                }
            }

            if (state.runningQueries ?: 0 > 0) {
                div {
                    +"Running queries: ${state.runningQueries}"
                }
            }

            state.allDelays?.let {
                val average = AverageDurationCalculator.calculateAverage(state.allDelays ?: emptyList())
                div {
                    +"Average duration: $average"
                }
            }
        }
    }

    /**
     * Called when the user clicks on the button
     */
    private fun onClick(maxDelayInput: INPUT?, requestCountInput: INPUT?) {
        val requestCount = extractRequestCount(requestCountInput)
        maxDelayInput?.let {
            if (it.value.isNotBlank()) {
                val delay: Int = try {
                    it.value.toInt()
                } catch (e: Exception) {
                    window.alert("Value is not an integer")
                    -1
                }
                if (delay > -1) {
                    // Start a coroutine for each request
                    repeat(requestCount) {
                        launch {
                            val duration = requestDelayedResource(delay, it)
                            updateViewForReceivedDuration(duration)
                        }
                    }
                }
            } else {
                window.alert("No integer value provided")
            }
        }
    }

    private fun extractRequestCount(requestCountInput: INPUT?): Int {
        return if (requestCountInput != null) {
            return requestCountInput.value.toInt()
        } else {
            1
        }
    }

    /**
     * Executes a request to the server. The request is suspendable. This is possible because it runs within a coroutine.
     */
    private suspend fun requestDelayedResource(maxDelay: Int, requestNr: Int): DurationDto {
        val request = XMLHttpRequest()
        request.open("POST", "http://${window.location.hostname}:8080/delayed", true)
        request.setJsonContentType().setJsonAccept()

        return suspendCoroutine { cont ->
            request.onerror = {
                cont.resumeWithException(Exception("Failed to command duration"))
            }

            request.onreadystatechange = {
                if (request.readyState == XMLHttpRequest.DONE) {
                    val validState: Short = 200
                    if (request.status == validState) {
                        log.info("Response: ${request.response}")
                        cont.resume(request.response.unsafeCast<DurationDto>())
                    } else {
                        cont.resumeWithException(Exception("There was going something wrong. Status: ${request.status} / ${request.statusText}"))
                    }
                }
            }
            val body = JSON.stringify(DelayedCmd(maxDelay, requestNr))
            request.send(body)
            resetStateForRequest()
            log.info("Query sent with max. duration: $maxDelay")
        }
    }

    private fun resetStateForRequest() {
        setState(clone(state).apply {
            runningQueries = (runningQueries ?: 0) + 1
        })
    }

    private fun updateViewForReceivedDuration(durationDto: DurationDto) {
        setState(kotlinext.js.clone(state).apply {
            runningQueries = if (runningQueries != null) {
                runningQueries!! - 1
            } else {
                0
            }
            allDelays = ArrayList(allDelays ?: emptyList()).apply { add(durationDto) }
        })
    }
}